# %%
import fiona
import numpy as np
import shapely.geometry as sg
import shapely.ops as so
from shapely.affinity import rotate, scale, translate

import pyeac.grid as grd
from pyeac.util import as_single_multipolygon, initial_cellsize

# %%


class GridMask:
    def __init__(self, shape=(4, 4)):
        self.shape = shape
        self.mask = np.ndarray(dtype=bool, shape=self.shape)
        self.mask[:] = False

    def get_formatted_mask(self):
        self.mask.shape = (16, 1)
        return np.repeat(self.mask, 2, axis=1)

    def apply_to(self, grid):
        if isinstance(grid, grd.Grid):
            masked = grid.coords[self.get_formatted_mask()]
        elif isinstance(grid, np.ndarray) and grid.shape == (16, 2):
            masked = grid[self.get_formatted_mask()]
        else:
            raise ValueError(
                "Either an instance of pyeac.grid.Grid or np.ndarray"
                f" of shape (16,2) is needed. Provided was:{type(grid)}"
            )
        return masked.reshape(int(masked.shape[0] / 2), 2)


class DiceMask(GridMask):
    def __init__(self):
        super().__init__()
        self.mask[0:2, 0:2] = True


class LMask(GridMask):
    def __init__(self):
        super().__init__()
        self.mask[:, 0] = True
        self.mask[3, 1] = True


class StrokeMask(GridMask):
    def __init__(self):
        super().__init__()
        self.mask[0, :] = True


# %%
def tetris_form(mask, grid, cellsize=1):
    return so.unary_union([grd.Regular(x, y, cellsize) for x, y in mask.apply_to(grid)])


def tetris_forms(grid, cellsize=1):
    dice = tetris_form(DiceMask(), grid, cellsize)
    L = tetris_form(LMask(), grid, cellsize)
    stroke = tetris_form(StrokeMask(), grid, cellsize)
    strokes = [stroke, rotate(stroke, angle=90)]
    Ls = [L, *[rotate(L, angle=angle) for angle in [90, 180, 270]]]
    Ls_mirrored = [scale(l, yfact=-1) for l in Ls]

    return [dice, *strokes, *Ls, *Ls_mirrored]


# %%
with fiona.open(
    "/home/phloose/workspace/EACM/VG250_LAN_ohne_gewaesser.geojson"
) as dataset:
    dland = list(dataset)


# %%
dland_area = as_single_multipolygon(dland).area
dland_feat_count = len(dland)
cellsize = initial_cellsize(dland_area, dland_feat_count)
grid = grd.RegularGrid(bounds=(0, 0, 4, 4), cellsize=1).coords * cellsize
# %%
forms = tetris_forms(grid, cellsize / 4)

# %%
bland_form_mapping = []
for bland_idx, bland in enumerate(dland):
    bland_geom = sg.shape(bland["geometry"]).buffer(0)
    form_intersection_areas = []
    for form_idx, form in enumerate(forms):
        form_translated = translate(form, bland_geom.centroid.x, bland_geom.centroid.y)
        # form_intersection_areas.append(form_translated.intersection(bland_geom).area)
        form_intersection_areas.append(bland_geom.intersection(form_translated).area)
    bland_form_mapping.append(
        (bland_idx, form_intersection_areas.index(min(form_intersection_areas)))
    )

# %%
sg.MultiPolygon([forms[idx] for _, idx in bland_form_mapping])
as_single_multipolygon(dland)

# %%
fitting_forms = [forms[idx] for _, idx in bland_form_mapping]
# %%
